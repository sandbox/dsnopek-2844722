/**
 * @file
 * JavaScript for UTM Token module.
 */

(function ($) {
  function getParams() {
    var match,
        pl     = /\+/g,  // Regex for replacing addition symbol with a space
        search = /([^&=]+)=?([^&]*)/g,
        decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
        query  = window.location.search.substring(1),
        urlParams = {};
    while (match = search.exec(query))
       urlParams[decode(match[1])] = decode(match[2]);
    return urlParams;
  }

  function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
  }

  function newCookie(name) {
    var utmParams = ['utm_source', 'utm_campaign', 'utm_medium'],
        urlParams = getParams(),
        cookie = {},
        has_param = false;

    $.each(utmParams, function () {
      if (typeof urlParams[this] !== 'undefined') {
        cookie[this] = urlParams[this];
        has_param = true;
      }
    });

    if (has_param) {
      cookie['first-visit'] = Math.floor((new Date()).getTime() / 1000);
      return name + '=' + encodeURIComponent($.param(cookie)) + ';expires=Fri, 31 Dec 9999 23:59:59 GMT;path=/';
    }
  }

  // If the cookie wasn't set, then set it.
  var cookie = getCookie('utm_token');
  if (!cookie) {
    cookie = newCookie('utm_token');
    if (cookie) {
      document.cookie = cookie;
    }
  }
})(jQuery);
